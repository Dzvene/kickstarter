<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@include file="../jspf/header.jspf" %>

<section class="main_block payment_block cf">
	<div class="breadcrumb">
		<ul class="breadcrumb_list cf">
			<li class="item f_left">
				<a href="/x/projects?category=1">Projects</a>
			</li>
			<li class="item  f_left"> /</li>
			<li class="item  f_left">
				<a href="projects_detail?project=${project.id}">Projects detail</a>
			</li>
		</ul>
	</div>
	<div class="content f_left">
		<h1>Инветирование в проект "${project.name}"</h1>

		<div class="block_wrapper">
			<form id="payment" class="payment" action="" method="post">
				<input type="hidden" name="project_id" value="${project.id}">
				<input type="hidden" name="investor_id" value="1">
				<input class="exist" type="hidden" name="exist" value="${project.exist}">

				<div class="check_bocs_block">
					<div class="input_wrap cf">
						<input class="invest5 f_left" type="radio" name="investCheck" value="5">
						<label class="f_left">Having invested 15% you will receive 5% of income</label>
					</div>
					<div class="input_wrap cf">
						<input class="invest7 f_left" type="radio" name="investCheck" value="7">
						<label class="f_left">Having invested 25% you will receive 7% of income</label>
					</div>
					<div class="input_wrap cf">
						<input class="invest25 f_left" type="radio" name="investCheck" value="10">
						<label class="f_left">Having invested 50% you will receive 10% of income</label>
					</div>
					<div class="input_wrap cf">
						<input class="invest50 f_left" type="radio" name="investCheck" value="30">
						<label class="f_left">Having invested 100% you will receive 30% of income</label>
					</div>
				</div>
				<div class="input_wrap">
					<label>Input your name</label>
					<input type="text">
				</div>
				<div class="input_wrap">
					<label>Input your card number</label>
					<input type="text">
				</div>
				<div class="input_wrap">
					<label>Input sum investing</label>
					<input class="invest" type="text" name="invest">
				</div>
				<div class="block_buttons">
					<input class="button button_chat" type="submit" value="Submit">
				</div>
			</form>
		</div>
	</div>
	<div class="right_sidebar f_right">
		<%@include file="../jspf/right_sidebar_categories.jspf" %>
	</div>
</section>


${category.id}


<%@include file="../jspf/footer.jspf" %>

