package nikfisher.kickstarter.dao;

import nikfisher.kickstarter.domain.Quote;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Random;

@Service
public class QuoteDAO {

	private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(QuoteDAO.class);

    @Autowired
	private SessionFactory sessionFactory;


	@SuppressWarnings("unchecked")
	@Transactional
	public String quoteGenerate() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Quote");
		List<Quote> motivators = query.list();

		if (motivators.size() > 0){
			int index = new Random().nextInt(motivators.size());
			return motivators.get(index).getQuote();
		}else {
			return "No records in the database";
		}

	}

	protected Session getSessionFactory() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}