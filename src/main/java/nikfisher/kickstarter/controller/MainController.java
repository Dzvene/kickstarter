package nikfisher.kickstarter.controller;

import nikfisher.kickstarter.dao.CategoriesDAO;
import nikfisher.kickstarter.dao.ProjectsDAO;
import nikfisher.kickstarter.dao.QuoteDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

	@Autowired
	private QuoteDAO quoteDAO;
	@Autowired
	private CategoriesDAO categoriesDAO;
	@Autowired
	private ProjectsDAO projectsDAO;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("quote", quoteDAO.quoteGenerate());
		model.addAttribute("categories", categoriesDAO.categories());
		return "pages/index";
	}

	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	public String projects(Model model) {
		model.addAttribute("quote", quoteDAO.quoteGenerate());
		model.addAttribute("categories", categoriesDAO.categories());
		model.addAttribute("projects", projectsDAO.projects());
		return "pages/projects";
	}

	@RequestMapping(value = "/projects_detail", method = RequestMethod.GET)
	public String projects_detail(Model model) {
		model.addAttribute("quote", quoteDAO.quoteGenerate());
		model.addAttribute("categories", categoriesDAO.categories());
		model.addAttribute("projects", projectsDAO.projects());
		return "pages/projects_detail";
	}

	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String news(Model model) {
		model.addAttribute("quote", quoteDAO.quoteGenerate());
		return "pages/news";
	}

	@RequestMapping(value = "/blog", method = RequestMethod.GET)
	public String blog(Model model) {
		model.addAttribute("quote", quoteDAO.quoteGenerate());
		return "pages/blog";
	}
}