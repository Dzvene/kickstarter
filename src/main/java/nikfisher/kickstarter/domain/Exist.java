package nikfisher.kickstarter.domain;

import javax.persistence.*;

@Entity
@Table(name = "cost_projects_has_users")
public class Exist {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "users_id")
	private String users_id;

	@Column(name = "exist")
	private String exist;

	@Column(name = "date_invest")
	private String date_invest;

	@Column(name = "projects_id")
	private String projects_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsers_id() {
		return users_id;
	}

	public void setUsers_id(String users_id) {
		this.users_id = users_id;
	}

	public String getExist() {
		return exist;
	}

	public void setExist(String exist) {
		this.exist = exist;
	}

	public String getDate_invest() {
		return date_invest;
	}

	public void setDate_invest(String date_invest) {
		this.date_invest = date_invest;
	}

	public String getProjects_id() {
		return projects_id;
	}

	public void setProjects_id(String projects_id) {
		this.projects_id = projects_id;
	}
}
