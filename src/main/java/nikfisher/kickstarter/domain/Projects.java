package nikfisher.kickstarter.domain;

import javax.persistence.*;

@Entity
@Table(name = "projects")
public class Projects {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "id_categories")
	private String id_categories;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "detail_description")
	private String detail_description;

	@Column(name = "dateStart")
	private String dateStart;

	@Column(name = "amount")
	private String amount;

	@OneToOne(cascade=CascadeType.ALL)
	@JoinTable(name="cost_projects_has_users",
			joinColumns={@JoinColumn(name="projects_id", referencedColumnName="id")})
	private Exist exist;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getId_categories() {
		return id_categories;
	}

	public void setId_categories(String id_categories) {
		this.id_categories = id_categories;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetail_description() {
		return detail_description;
	}

	public void setDetail_description(String detail_description) {
		this.detail_description = detail_description;
	}

	public String getDateStart() {
		return dateStart;
	}

	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}

	public Exist getExist() {
		return exist;
	}

	public void setExist(Exist exist) {
		this.exist = exist;
	}
}
