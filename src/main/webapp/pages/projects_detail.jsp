<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="../jspf/header.jspf" %>

<section class="main_block project_detail cf">
	<div class="breadcrumb">
		<ul class="breadcrumb_list cf">
			<li class="item  f_left">
				<a href="projects">Projects</a>
			</li>
		</ul>
	</div>
	<div class="content f_left">
	<%--<c:forEach items="${projects}" var="project" varStatus="loop">--%>
		<div class="title_wrap">
			<h1><c:out value="${project.name}"/></h1>
		</div>
		<div class="projects_wrap">
			<div class="project">
				<div class="block_wrapper">
					<h3>Description</h3>

					<p>
						<c:out value="${project.detail_description}"/>
					</p>
				</div>
				<div class="block_wrapper">
					<h3>History</h3>

					<p>
						<%--<c:out value="${project.history}"/>--%>
					</p>
				</div>
				<div class="block_wrapper">
					<p>
						<%--<a href="${project.video}" target="_blank">Video demo</a>--%>
					</p>
				</div>
				<div class="info_block">
					<p><span class="bold">Amount:</span> <c:out value="${project.amount}"/></p>

					<p><span class="bold">Exist:</span> <c:out value="${project.exist}"/></p>

					<p><span class="bold">Days remaining:</span> <c:out value="${project.dateStart}"/></p>
				</div>
				<div class="block_buttons">
					<a href="investing?project=${project.id}" class="button button_invest">Investing</a>
				</div>
				<br>
			</div>

			<%--</c:forEach>--%>

			<div class="chat">
				<div class="chat_questions">

					<%--<c:forEach items="${chat}" var="chat" varStatus="loop">--%>
						<%--<div class="chat_elem">--%>
							<%--<p><c:out value="${chat.questions}"/></p>--%>

							<%--<div class="info_block">--%>
								<%--<p><span class="bold">Name:</span> <c:out value="${chat.nameAutor}"/></p>--%>

								<%--<p><span class="bold">Date:</span> <c:out value="${chat.date}"/></p>--%>
							<%--</div>--%>
						<%--</div>--%>
					<%--</c:forEach>--%>

				</div>

				<br>

				<form id="chat_form" class="chat_form" action="/x/projects_detail?project=${project.id}"
					  method="post">
					<%--<input type="hidden" name="project_id" value="${project.id}">--%>
					<input type="hidden" name="author_id" value="1">

					<div class="input_wrap">
						<label>Input your message</label>
						<textarea class="input_chat" name="chat_value"></textarea>
					</div>
					<div class="block_buttons">
						<input class="button button_chat" type="submit" value="Submit">
					</div>
				</form>
			</div>
		</div>


	</div>
	<div class="right_sidebar f_right">
		<%@include file="../jspf/right_sidebar_categories.jspf" %>
	</div>
</section>

<%@include file="../jspf/footer.jspf" %>
