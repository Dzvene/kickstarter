(function ($) {

	$(document).ready(function () {
		formInvestAutoFildInput();
	});

	function formInvestAutoFildInput() {

		var exist = $('.exist').val();

		$(".check_bocs_block input").click(function () {
			var valueBonus = $(".check_bocs_block :input:checked").val();
			var procentBonus = exist / 100 * valueBonus;
			$('.invest').val(procentBonus);
		});
	}
}(jQuery));