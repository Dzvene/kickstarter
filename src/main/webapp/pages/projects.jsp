<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="../jspf/header.jspf" %>

<section class="main_block cf">
	<div class="content f_left">
		<div class="title_wrap">
			<h1>Projects</h1>
		</div>
		<div class="projects_wrap">
			<c:forEach items="${projects}" var="project" varStatus="loop">


				<div class="project">
					<div class="block_wrapper">
						<h3>
							<a href="projects_detail?project=${project.id}">
								<c:out value="${project.name}"/>
							</a>
						</h3>
					</div>
					<div class="block_wrapper">
						<p>
							<c:out value="${project.description}"/>
						</p>
					</div>
					<div class="info_block">
						<p>
							<span class="bold">Amount:</span>
							<c:out value="${project.amount}"/>
						</p>
						<p>
							<span class="bold">Exist:</span>
							<c:out value="${project.exist}"/>
						</p>
						<p>
							<span class="bold">Days remaining:</span>
							<c:out value="${project.dateStart}"/>
						</p>
					</div>
					<br>
					<a href="projects_detail?projects_detail=${project.id}" class="button">Read more</a>
					<br>
				</div>

			</c:forEach>
		</div>
	</div>
	<div class="right_sidebar f_right">
		<%@include file="../jspf/right_sidebar_categories.jspf" %>
	</div>
</section>

<%@include file="../jspf/footer.jspf" %>