package nikfisher.kickstarter.dao;

import nikfisher.kickstarter.domain.Categories;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CategoriesDAO {

	private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CategoriesDAO.class);

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Categories> categories() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Categories");
		List<Categories> categories = query.list();

		return categories;

	}

	protected Session getSessionFactory() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
